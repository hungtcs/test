import './App.css'
import { NavLink, Outlet } from 'react-router-dom'

function App() {

  return (
    <>
      <header>
        <nav>
          <ul>
            <li><NavLink to='/'>Home</NavLink></li>
            <li><NavLink to='/list'>List</NavLink></li>
          </ul>
        </nav>
      </header>
      <Outlet />
    </>
  )
}

export default App
